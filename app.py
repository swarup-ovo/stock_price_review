from alembic import script
from flask import Flask, render_template, request, redirect, session, g, url_for
from flask_sqlalchemy import SQLAlchemy
from datetime import datetime
from werkzeug.security import generate_password_hash, check_password_hash
from flask_migrate import Migrate, history
import requests


app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = "sqlite:///local.db"
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
db = SQLAlchemy(app)
app.secret_key = 'somesecretkeythatonlyishouldknow'
migrate=Migrate(app,db) #Initializing migrate.

##s

##modele for role master
class role(db.Model):
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    role_title = db.Column(db.String(100), unique=True, nullable=False)
    role_description = db.Column(db.String(500), nullable=False)
    date_created = db.Column(db.DateTime, default=datetime.utcnow)

    def __repr__(self) -> str:
        return f"{self.id} - {self.role_title}"

#declaring the user model
class User(db.Model):
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    first_name = db.Column(db.String(100), unique=False, nullable=True)
    last_name = db.Column(db.String(100), unique=False, nullable=True)
    username = db.Column(db.String(80), unique=False, nullable=True)
    password = db.Column(db.String(255), nullable=True)
    email = db.Column(db.String(120), unique=True, nullable=True)
    contact_no = db.Column(db.String(15), unique=True, nullable=True)
    is_admin = db.Column(db.Boolean, default=False)
    date_created = db.Column(db.DateTime, default=datetime.utcnow)
    role = db.relationship('EmployeeRole', backref="emp_role", lazy=True)


    def __repr__(self) -> str:
        return f"{self.id} - {self.username}"
    ## method to get the full name
    def full_name(self):
        return self.first_name + ' ' + self.last_name 

    ## method to get all the assign role to each employee
    def roles(self):
        assign_role =EmployeeRole.query.filter_by(employee=self.id)
        final_str = ""
        for each in assign_role:
            role_title = role.query.filter_by(id=each.role)
            final_str = final_str + role_title[0].role_title + ","
        return final_str


##model for representing the relation between role and employee
class EmployeeRole(db.Model):
    id = id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    employee = db.Column(db.Integer, db.ForeignKey('user.id'), nullable=False)
    role = db.Column(db.Integer, db.ForeignKey('role.id'), nullable=False)

    def __repr__(self) -> str:
        return f"{self.id}"

##model for employee stock search history
class EmployeeStockSearchHistory(db.Model):
    id = id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    employee = db.Column(db.Integer, db.ForeignKey('user.id'), nullable=False)
    script = db.Column(db.String(100), nullable=True)
    date_created = db.Column(db.DateTime, default=datetime.utcnow)

    def __repr__(self) -> str:
        return f"{self.id}"

    #return related employee full name
    def search_by(self):
        searched_by = User.query.filter_by(id=self.employee).first()
        return searched_by.full_name()


##defining the model to store the stock search history data
class EmployeeStockSearchHistoryData(db.Model):
    id = id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    script = db.Column(db.Integer, db.ForeignKey('employee_stock_search_history_data.id'), nullable=False)
    open_price = db.Column(db.String(100), nullable=True)
    high_price = db.Column(db.String(100), nullable=True)
    low_price = db.Column(db.String(100), nullable=True)
    close_price = db.Column(db.String(100), nullable=True)
    date_created = db.Column(db.DateTime, default=datetime.utcnow)

    def __repr__(self) -> str:
        return f"{self.id}"

    #return related script name
    def script_name(self):
        searched_by = EmployeeStockSearchHistory.query.filter_by(id=self.script).first()
        return searched_by.script
    
    def search_time(self):
        searched_by = EmployeeStockSearchHistory.query.filter_by(id=self.script).first()
        return str(searched_by.date_created).split('.')[0]

    
############################################## SEARCH STOCK VIEW ##################################################################
##serach stock price
@app.route('/search/stock', methods=['GET', 'POST'])
def search_stock():
    script_name = None
    # data = list()
    final_lst = list()
    if request.method=='POST':
        script_name = request.form['script_name']
        # session.pop('user_id', None)
        if session['user_id']:
            # storing the search history details
            
            new_search = EmployeeStockSearchHistory(employee=session['user_id'], script=script_name)
            db.session.add(new_search)
            db.session.commit()
            # declaring the final end point for stock data
            
            primary_end_point = "https://www.alphavantage.co/query?function=TIME_SERIES_INTRADAY&symbol={}&interval=5min&outputsize=full&&apikey=28ZQGJ4EVEJ8ZSF5".format(script_name)
            # get request to the alphavantage realtime stock data api
            
            response = requests.get(primary_end_point, verify=False,timeout=10)
            if response.status_code == 200:
                # data filter from api response
                data1 = response.json()
                ##using the try except block to bypass the company not found issue
                try:
                    data = list(data1['Time Series (5min)'])
                    data2 = data1['Time Series (5min)']
                    final_lst = []
                    for keys in data[0:10]:
                        final_lst.append(data2[keys])
                        # storing the company stock data
                        print("new_search", new_search)

                        searched_data = EmployeeStockSearchHistoryData(script=int(new_search.id),open_price=str(data2[keys]['1. open']), 
                        high_price=str(data2[keys]['2. high']), low_price=str(data2[keys]['3. low']), close_price=str(data2[keys]['4. close']) )
                        # print(searched_data)
                        db.session.add(searched_data)
                        db.session.commit()
                except:
                    history = EmployeeStockSearchHistory.query.all() 
                    # print(history)
                    return render_template('company_stock_pricing.html', history=history, Pricing=final_lst, name=script_name) 

                
            else:
                history = EmployeeStockSearchHistory.query.all() 
                print(history)
                return render_template('company_stock_pricing.html', history=history, Pricing=final_lst, name=script_name)  

        
    history = EmployeeStockSearchHistory.query.all() 
    print(history)
    if 'user_id' in session:
        return render_template('company_stock_pricing.html', history=history, Pricing=final_lst, name=script_name)
    else:
        return render_template('employee_login.html')
    

##stock price data
@app.route('/stock/data/<int:id>', methods=['GET'])
def query_strings(id):
    if request.method == "GET":
        history_data = id
        ## queryset to filter the stock search history data
        data = EmployeeStockSearchHistoryData.query.filter_by(script=history_data).all()
        name = data[0].script_name() if data else None
        search_time=data[0].search_time() if data else None
        if 'user_id' in session:
            return render_template('stock_history_data.html', Pricing=data, name=name, search_time=search_time)
        else:
            return render_template('employee_login.html')
        


##delete search history
@app.route('/stock/search/delete/<int:id>',methods=['GET'])
def delet_search_history(id):
    if request.method=='GET':
        print(request.method)
        if EmployeeStockSearchHistory.query.filter_by(id=id).first() is not None:
            print("in")
            role_obj = EmployeeStockSearchHistory.query.filter_by(id=id).one()
            db.session.delete(role_obj)
            db.session.commit()

    if 'user_id' in session:
        return redirect("/search/stock")
    else:
        return render_template('employee_login.html')    
    



############################################## ROLE CRUD VIEW ##################################################################

## add role
@app.route('/admin/add/role', methods=['GET', 'POST'])
def add_role():
    if request.method=='POST':
        role_title = request.form['role_title']
        role_description = request.form['role_description']
        print(role_title, role_description)
        
        predefine_role = role.query.filter_by(role_title=role_title).first()
        if predefine_role:
            allrole = role.query.all()
            message = " Not able to add .Role title should be unique"
            return render_template('role_add.html', allrole=allrole, message=message)

        new_role = role(role_title=role_title, role_description=role_description)
        db.session.add(new_role)
        db.session.commit()
        
    allrole = role.query.all() 
    print(allrole)
    message = None
    if 'user_id' in session:
        return render_template('role_add.html', allrole=allrole, message=message)
    else:
        return render_template('employee_login.html')


## view for update the role master
@app.route('/admin/update/role/<int:id>', methods=['GET', 'POST'])
def role_update(id):
    if request.method=='POST':
        role_title = request.form['role_title']
        role_description = request.form['role_description']
        print(role_title, role_description)
        predefine_role = role.query.filter_by(id=id).first()
        predefine_role.role_title = role_title
        predefine_role.role_description = role_description
        db.session.add(predefine_role)
        db.session.commit()
        print("it's working")
        print("in if")
        return redirect(url_for('add_role'))
    print("out if")
    update_role = role.query.filter_by(id=id).first()
    if 'user_id' in session:
        return render_template('role_update.html', update_role=update_role)
    else:
        return render_template('employee_login.html')


##delete role
@app.route('/admin/delete/role/<int:id>',methods=['GET'])
def deleterole(id):
    if request.method=='GET':
        print(request.method)
        if role.query.filter_by(id=id).first() is not None:
            print("in")
            role_obj = role.query.filter_by(id=id).one()
            db.session.delete(role_obj)
            db.session.commit()
    if 'user_id' in session:
        return redirect("/admin/add/role")
    else:
        return render_template('employee_login.html')    
    



############################################## ADD EMPLOYEE VIEW ##################################################################
##register employee
@app.route('/admin/add/employee', methods=['GET', 'POST'])
def register_employee():
    if request.method=='POST':
        first_name = request.form['first_name']
        last_name = request.form['last_name']
        username = first_name + '@' +last_name + str(len(first_name+last_name))
        email = request.form['email'] if request.form['email'] else None

        password = request.form['password']
        hashed_password = generate_password_hash(password, method='sha256') if password else None
        contact_no = request.form['contact_no'] if request.form['contact_no'] else None
        roles = request.form.getlist('role')
        print(roles)
        duplicate_contact_no_check = User.query.filter_by(contact_no=contact_no).first()
        duplicate_email_check = User.query.filter_by(email=email).first()
        
        ## checking for duplicate contact no and email_id
        if duplicate_contact_no_check or duplicate_email_check:
            all_eemployee = User.query.all() 
            all_role = role.query.all()
            message = "Duplicate contact_no  and email are not allowed"
            return render_template('add_employee.html', all_eemployee=all_eemployee, all_role=all_role, message=message )

        
        new_employee = User(first_name=first_name, last_name=last_name, username=username, email=email, contact_no=contact_no, password=hashed_password)
        print(new_employee)
        db.session.add(new_employee)
        db.session.commit()
        obj = User.query.all()
        employee_id = obj[-1].id
        ## adding the employee role through this iteration
        for each_role in roles:
            print(each_role)
            role_assign = EmployeeRole(employee=int(employee_id),role=int(each_role))
            db.session.add(role_assign)
            db.session.commit()
        
        
    all_eemployee = User.query.all() 
    all_role = role.query.all()
    print(all_role)
    if 'user_id' in session:
        return render_template('add_employee.html', all_eemployee=all_eemployee, all_role=all_role )
    else:
        return render_template('employee_login.html')


############################################## SESSION CHECK ##################################################################
## execute before requesting every endpoint to check that the user is still logged in or not
@app.before_request
def before_request():
    g.user = None
    users = User.query.all()
    if 'user_id' in session:
        try:
            user = [x for x in users if x.id == session['user_id']]
            if user:
                g.user = user
        except:
            pass

############################################## EMPLOYEE LOGIN VIEW ##################################################################      

@app.route('/login', methods=['GET', 'POST'])
def login():
    if request.method == 'POST':
        print("in login")
        session.pop('user_id', None)

        username = request.form['username']
        password = request.form['password']
        users = User.query.filter_by(username=username)
        user = [x for x in users if x.username == username]
        if user:
            user = user[0]
            # print(check_password_hash(user.password, password))
            if user and check_password_hash(user.password, password):
                session['user_id'] = user.id
                print("in if")
                # print(user)
                if user.is_admin:
                    return redirect(url_for('register_employee',  user=user.is_admin))
                else:
                    return redirect(url_for('search_stock',  user=user.is_admin))


            else:
                message = "the username and password you entered is incorrect"
            return render_template('employee_login.html', message=message)
        else:
            message = "the username and password you entered is incorrect"
            return render_template('employee_login.html', message=message)

            
        

    return render_template('employee_login.html')

##setup the default route
@app.route('/')
def profile():
    if not g.user:
        return redirect(url_for('login'))

    return render_template('add_employee.html')


############################################## LOGOUT VIEW ##################################################################
@app.route('/logout')
def logout():
    session.pop('username',None)
    session.pop('user_id',None)
    return redirect(url_for('login'))



if __name__ == "__main__":
    app.run(debug=False)