STEP 1: First create the virtual environment
	1. pip install virtualenv 
	2. virtualenv venv (Here venv is the environment name)
	3. Activate the environment--
		3.1 venv\Scripts\activate (For windows user)
		3.2 source venv/bin/activate (for linux user)


STEP 2: Back to the project directory and download the dependencies
	1. Pip install -r requirements.txt

step 3. After successfully install all the dependencies now run the app.
	1. python app.py